#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

echo "Script makes pets database with cats table in mysql, then copies php file to selected document root and test connection."
echo ""

echo "Enter mysql username."
read username

echo "Enter mysql password."
read -s password

mysql -u $username -p -e "CREATE DATABASE pets; USE pets; CREATE TABLE cats ( id INT unsigned NOT NULL AUTO_INCREMENT, name VARCHAR(150) NOT NULL, owner VARCHAR(150) NOT NULL, birth DATE NOT NULL, PRIMARY KEY (id) ); INSERT INTO cats ( name, owner, birth) VALUES ( 'Sandy', 'Lennon', '2015-01-03' ), ( 'Cookie', 'Casey', '2013-11-13' ), ( 'Charlie', 'River', '2016-05-21' );"

echo "Database created!"

echo
echo "Enter document root to put cats.php file to. If empty /var/www/html will be used."
read docroot

if [ "$docroot" = "" ]; then
  docroot="/var/www/html"
fi

cat <<EOF > $docroot/cats.php
<html style="background-color: #777">
  <head>
    <title>Test cats</title>
  </head>
  <body style="font-family: monospace">

    <p>Test connection to local mysql:</p>

    <?php
    \$servername = "localhost";
    \$username = "$username";
    \$password = "$password";
    \$dbname = "pets";

    // Create connection
    \$conn = new mysqli(\$servername, \$username, \$password, \$dbname);
    // Check connection
    if (\$conn->connect_error) {
        die("Connection failed: " . \$conn->connect_error);
    }

    \$sql = "SELECT id, name, owner FROM cats";
    \$result = \$conn->query(\$sql);

    if (\$result->num_rows > 0) {
        // output data of each row
        while(\$row = \$result->fetch_assoc()) {
            echo "id: " . \$row["id"]. " - Name: " . \$row["name"]. " " . \$row["owner"]. "<br>";
        }
    } else {
        echo "0 results";
    }
    \$conn->close();
    ?>

  </body>
</html>
EOF

echo "Enter mysql pass to delete database and pets.php file!"
mysql -u $username -p -e "drop database pets;"
rm $docroot/cats.php
exit 0
