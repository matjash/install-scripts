#!/bin/bash
echo "Script will install ZSH bash with oh-myzsh template + zsh-syntax-highlighting + z.sh"

#if [ $EUID -ne 0 ]; then
#   echo "This script must be run as root" 
#   exit 1
#fi

############### ARGS ######################

user=$1

# If user is given enable silent installation
if [ "$1" != "" ]; then
  silent="-y"
fi

if [ "$user" = "" ]; then
  echo "Enter username to install ZSH for"
  read user
fi

if [ ! $(getent passwd $user) ]; then
  echo "No user found with name $user"
  exit 1
fi

############ FUNCTIONS ####################


function InstallZSH {

  # install zsh
  command -v zsh >/dev/null 2>&1 || { echo >&2 "ZSH is required but it's not installed.."; apt-get install zsh $silent; }

}

function ohmyzsh {

  # Skip if oh-my-zsh found in user dir
  if [ -d /home/$user/.oh-my-zsh ]; then

    echo ".oh-my-zsh dir found in user directory exiting..."

  else

    echo "Add oh-my-zsh template"
    #curl -L http://install.ohmyz.sh | bash
    ./.zsh_script.sh $user
    mkdir -p /home/$user/.zshextra/

    # Add zsh-syntax-highlighting
    git clone git://github.com/zsh-users/zsh-syntax-highlighting.git /home/$user/.zshextra/zsh-syntax-highlighting

    # Add z.sh last paths - https://github.com/rupa/z/blob/master
    curl https://raw.githubusercontent.com/rupa/z/master/z.sh >> /home/$user/.zshextra/z.sh

    # Alternative copy template file
    cat /home/$user/.oh-my-zsh/themes/mh.zsh-theme >>  /home/$user/.zshrc


    ## Enable zsh-syntax-highlighting
    echo "" >> /home/$user/.zshrc
    echo "# My configuration" >> /home/$user/.zshrc
    echo 'source ~/.zshextra/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >> /home/$user/.zshrc
    echo "# Z path" >> /home/$user/.zshrc
    echo "export ZSHEXTRA=/home/$user/.zshextra" >> /home/$user/.zshrc
    echo '. ${ZSHEXTRA}/z.sh' >> /home/$user/.zshrc
    echo "# Aliases" >> /home/$user/.zshrc
    echo "alias ll=\"ls -la\"" >> /home/$user/.zshrc
    echo "alias -g NULL=\"> /dev/null 2>&1\""
    sed -i 's,# CASE_SENSITIVE="true",CASE_SENSITIVE="true",g' /home/$user/.zshrc
    # sed -i 's,# HYPHEN_INSENSITIVE="true",HYPHEN_INSENSITIVE="true",g' /home/$user/.zshrc
    sed -i 's,# DISABLE_AUTO_TITLE="true",DISABLE_AUTO_TITLE="true",g' /home/$user/.zshrc
    # sed -i 's,# ENABLE_CORRECTION="true",ENABLE_CORRECTION="true",g' /home/$user/.zshrc

    # Customize theme
    sed -i 's,fg\[red\],fg\[yellow\],g' /home/$user/.zshrc
  
    # Add machine name in same color
    sed -i 's,}:%{$fg\[,}@%m:%{$fg\[,g' /home/$user/.zshrc
    sed -i 's,$UID -eq 0,$UID -eq 1000,g' /home/$user/.zshrc

  fi

}

function AllowChangeBash {

  if [ $(cat /etc/pam.d/chsh | grep "group=chsh") ]; then
    echo "PAM modification not needed."
    exit 1
  else

    echo "Allow user to change bash without sudo"

    # make backup
    if [ ! -f /etc/pam.d/chsh.bak ]; then
      cp /etc/pam.d/chsh /etc/pam.d/chsh.bak
    fi

    cat >> ./pam.add <<EOF
# This allows users of group chsh to change their shells without a password.
auth       sufficient   pam_wheel.so trust group=chsh

EOF

    (cat ./pam.add; cat /etc/pam.d/chsh) > ./pam.new
    mv ./pam.new /etc/pam.d/chsh
    rm ./pam.add

    groupadd chsh

  fi

  usermod -a -G chsh $user

  # Change shell
  binZSH=$(command -v zsh)
  echo "ZSH path is: $binZSH"
  sudo -u $user chsh -s $binZSH

  echo "Enter zsh or restart shell to take changes!"

}

function FixPermissions {

  chown $user:$user /home/$user -R

}


function Purge {

  binBASH=$(command -v bash)
  sudo -u $user chsh -s $binBASH

  sed -i "s,# This allows users of group chsh to change their shells without a password.,,g" ./pam.test

  rm -rf /home/$user/.zshextra
  rm -rf /home/$user/.oh-my-zsh
  rm -rf /home/$user/.z
  rm /home/$user/.zsh_history
  rm /home/$user/.z
  find /home/$user/ -name ".zcomp*" -exec rm {} \;
  find /home/$user/ -name ".zsh*" -exec rm {} \;

  sed -i "/# This allows users of group chsh to change their shells without a password/d" /etc/pam.d/chsh
  sed -i "/auth       sufficient   pam_wheel.so trust group=chsh/d" /etc/pam.d/chsh

  apt-get purge zsh $silent

  echo ZSH purged!
  exit 1

}

################### LOGIC ###################

# Purge
if [ "$2" = "purge" ]; then
  
  Purge

fi

# Install ZSH
if [ "$1" = "" ]; then

  echo "Install ZSH?"
  read yes

  if [ "$yes" = "y" ]; then 
    InstallZSH
    yes=""
  fi

else
  InstallZSH
fi

# Install oh-my-zsh
if [ "$1" = "" ]; then

  echo "Install oh-my-zsh?"
  read yes

  if [ "$yes" = "y" ]; then
    ohmyzsh
    yes=""
  fi

else
  ohmyzsh
fi

# Allow change of bash
if [ "$1" = "" ]; then

  echo "Change bash?"
  read yes

  if [ "$yes" = "y" ]; then
    AllowChangeBash
    yes=""
  fi

else
  AllowChangeBash
fi

# Fix permissions of home folder
if [ "$1" = "" ]; then

  echo "Fix permissions of home folder?"
  read yes

  if [ "$yes" = "y" ]; then
    FixPermissions
    yes=""
  fi

else
  FixPermissions
fi
