#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

#sed -i "s,#AuthorizedKeysFile,AuthorizedKeysFile,g" /etc/ssh/sshd_config

user=$1

while [ "$user" = "" ]; do
  echo "Username is required:"
  read user
done

key=$2

########### FUNCTIONS ############

function createUser {

  useradd -m -s /bin/bash $user

  mkdir -p /home/$user/.ssh
  chmod 700 /home/$user/.ssh
  echo $key >> /home/$user/.ssh/authorized_keys
  chmod 600 /home/$user/.ssh/authorized_keys
  chown $user:$user /home/$user/.ssh/ -R

} 

function deleteUser {

  deluser $user

}

########### LOGIC ##################

if [ "$2" = "delete" ]; then

  echo "$user and /home/$user folder will be removed 'y' to continue..."
  read yes

  if [ "$yes" = "y" ]; then

    deleteUser
    rm -rf /home/$user
    yes=""
    echo "User deleted!"

  fi

 exit 0

fi

createUser
