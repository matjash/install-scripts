#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root"
  exit 1
fi

cmnd=$1
name=$2
username=$3
ip=$4

if [ "$3" = "" ]; then
  username=$name
fi

if [ "$3" = "append" ] || [ "$4" = "append" ]; then
  append=true
  username=$name
fi

function Help {

 cat << EOF
Script to help generate and copy ssh keys:
  - first parameter is command:
    - generate
      - second argument name of ssh key pair
      - third argument is username if different then name 
      - if last argument is append generated key is appended to user authorized keys.

    EXAMPLE: ./ssh.sh generate deploy_id deploy

    Generates /home/deploy/.ssh/deploy_id keypair.

    - config (copies config file from given user to selected user)
      - user to copy from
      - user to copy to

   EXAMPLPE: sudo ./ssh.sh config ubuntu deploy

   Copy /home/ubuntu/.ssh/config to /home/deploy/.ssh/config and renames user to deploy in file
EOF

}

function Append {

  cat /home/$username/.ssh/$name.pub >> /home/$username/.ssh/authorized_keys
  cat <<EOF

Key appended!

EOF

  cat /home/$username/.ssh/authorized_keys

}

function Generate {

  echo "Generate ssh key without password, saves it to user .ssh folder"

  ssh-keygen -b 2048 -t rsa -f /home/$username/.ssh/$name -q -N ""
  sed -i "s,root@$(hostname -s),$name,g" /home/$username/.ssh/$name.pub

  chown $username:$username /home/$username/.ssh/$name /home/$username/.ssh/$name.pub

  # Show
  ls  /home/$username/.ssh/
  #cat /home/$username/.ssh/$name.pub
  echo "Key generated in /home/$username/.ssh/ folder!"

  if [ $append ]; then

    Append

  fi 

}


############# LOGIC ###############

if [ "$1" = "" ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
  Help
fi

if [ "$cmnd" = "generate" ]; then
 
  Generate

fi

if [ "$cmnd" = "config" ]; then
  cp /home/$name/.ssh/config /home/$username/.ssh/config
  sed -i "s,$name,$username,g" /home/$username/.ssh/config 
  chown $username:$username /home/$username/.ssh/config
fi
