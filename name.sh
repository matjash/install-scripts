#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

if [ "$1" = "" ]; then
  echo "Change server name."
  read name
else
  name=$1
fi

oldhostname=$(hostname -s)
hostname $name
bash -c "echo '$name' > /etc/hostname"
sed -i "s,127.0.0.1 localhost,127.0.0.1 $name,g" /etc/hosts
sed -i "s,127.0.0.1 $oldhostname,127.0.0.1 $name,g" /etc/hosts

echo "Hostaname set as: $name"
echo "Reload terminal or load in new to take changes."

exit 0
