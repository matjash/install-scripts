#!/bin/bash

Source="/var/www/cs-prod-api/storage/app/users"
Target="backup:/home/ubuntu/backup/cs-prod-api/storage"
Identity="/home/ubuntu/.ssh/backup"

SCRIPTPATH=$(readlink -f $0)


if [ "$Source" = "" ] || [ "$Target" = "" ] || [ "$Identity" = "" ]; then
 echo "No source or target set! Set them here $SCRIPTPATH!"
  exit 1
fi

function startAgent() {
  #Start ssh agent and add key
  eval $(ssh-agent -s)
  ssh-add $Identity 2>/dev/null
}

function killAgent() {
  kill $SSH_AGENT_PID
}

function rsyncStart () {
  # If no args then exit
  [[ -z "$1" ]] && echo "No argument supplied $1" && exit 1
  startAgent
  rsync -$1 --delete $Source $Target #> /dev/null 2>&1
  killAgent
}

function cronjob() {
  comment="# Cron job for script $0"
  crontab -l > ./tempcron
  echo "$comment" >> ./tempcron
  echo "0 0 * * * $SCRIPTPATH $1 >> $SCRIPTPATH.log 2>&1" >> ./tempcron
  crontab ./tempcron
  rm ./tempcron
  echo Cron added!
  crontab -l
  exit 0
}

if [ "$1" = "cron" ]; then
  [[ -z "$2" ]] && echo "No argument supplied $2 as arg2 like 'rvz'" && exit 1
  cronjob $2
fi

rsyncStart $1
