#!/bin/bash
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

echo "Script makes swap file in /swapfile, default ammount is 1024MB, 1st arg is size in MB, 2nd is swap file location. Working, don't exit till finished."

size=$1
file=$2

if [ "$1" = "" ]; then
  size=1024
fi

if [ "$2" = "" ]; then
  file="/swapfile"
fi


dd if=/dev/zero of=$file bs=1M count=$size
mkswap $file
chmod 600 $file
swapon $file
echo "$file swap swap defaults 0 0" | sudo tee -a /etc/fstab >/dev/null

top -n 1 | grep Swap
exit 0
